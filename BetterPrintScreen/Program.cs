﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterPrintScreen
{
    static class Program
    {
        private static KeyEventHandler gkh_KeyUp;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            KeyboardHook gkh;
            gkh = new KeyboardHook();
            gkh.HookedKeys.Add(Keys.PrintScreen);
            //gkh.KeyDown += new KeyEventHandler(gkh_KeyDown);
            gkh.KeyUp += new KeyEventHandler(gk_KeyUp);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run();
        }

        static void gk_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PrintScreen)
            {
                Form1 frm = new Form1();
                frm.Show();
            }
        }

    }
}
