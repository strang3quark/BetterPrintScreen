﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterPrintScreen
{
    public partial class Form1 : Form
    {
 
        Image clipboardImage;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            btnSave.Hide();

            this.MaximumSize = new Size(Screen.FromControl(this).Bounds.Width, Screen.FromControl(this).Bounds.Height);

            captureClipboardImage();
        }


        private void captureClipboardImage()
        {
            clipboardImage = Clipboard.GetImage();
            string tempFolder = Path.GetTempPath();
            string imagePath = Path.Combine(tempFolder, "imagefile.jpg");

            pictureBox1.Image = clipboardImage;
            //ResizeWidth(800, clipboardImage.Width, clipboardImage.Height);
            ResizeHeight(500, clipboardImage.Width, clipboardImage.Height);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //saveClipboardImage();
            String defaultName = "Screenshot " +
                DateTime.Now.Year.ToString() + "-" +
                DateTime.Now.Month.ToString("D2") + "-" +
                DateTime.Now.Day.ToString("D2") + "-" +
                DateTime.Now.Hour.ToString("D2") + "_" +
                DateTime.Now.Minute.ToString("D2");
            saveFileDialog1.Title = "Save Screenshot";
            saveFileDialog1.Filter = "PNG Image|*.png";
            saveFileDialog1.FileName = defaultName;
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != defaultName && saveFileDialog1.FileName != "")
            {
                clipboardImage.Save(saveFileDialog1.FileName,System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        public void ResizeWidth(int newWidth, int currentWidth, int currentHeight)
        {
            this.Height = (int)((double)newWidth / (double)((double)currentWidth / (double)currentHeight));
            this.Width = newWidth;
        }

        public void ResizeHeight(int newHeight, int currentWidth, int currentHeight)
        {
            this.Width = (int)((double)newHeight * (double)((double)currentWidth / (double)currentHeight));
            this.Height = newHeight;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Location.Y >= this.Height - 100)
            {
                btnSave.Show();
            }
            else
            {
                btnSave.Hide();
            }
        }
    }
}
