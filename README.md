# BetterPrintScreen
PrintScreen save dialog for Windows

This is just a simple software that shows a Save Dialog after pressing PrintScreen Key

## After taking screenshot

![After taking screenshot](http://i.imgur.com/CllMeZo.png)


## Show Save Button on Hover Bottom

![Show Save Button on Hover Bottom](http://i.imgur.com/88LR3KA.png)

## How to run it
Just execute the BetterScreenShot.exe and you are good to go.

[Click here to download](https://github.com/strang3quark/BetterPrintScreen/releases/download/v0.1/BetterPrintScreen.exe)
